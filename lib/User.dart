class User {
  String profileImage;
  String name;
  String bio;

  User({this.profileImage, this.name, this.bio});
}