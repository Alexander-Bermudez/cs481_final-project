class Product {
  final String image, title, desc;
  final int size, id;
  final double price;
  bool addedToCart;

  Product(
  {
    this.image,
    this.title,
    this.desc,
    this.price,
    this.size,
    this.id,
    this.addedToCart
  });
}