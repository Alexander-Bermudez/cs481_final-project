import 'package:final_project/product/Product.dart';
import 'package:flutter/material.dart';
import 'package:final_project/product/ProductList.dart';


class ProductReviewPage extends StatefulWidget{

  final Product product;

  const ProductReviewPage({
    Key key,
    this.product
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return ProductReviewPageState();
  }

}

class ProductReviewPageState extends State<ProductReviewPage> {


  @override
  ProductReviewPage get widget => super.widget;

  @override
  Widget build(BuildContext context) {

     return Scaffold(
         appBar: AppBar(
         centerTitle: true,
         title: Text('Totally not Amazon'),
    actions: <Widget>[
    PopupMenuButton(
    itemBuilder: (context) => [
    PopupMenuItem(
    child: Text('Sign In'),
    ),
    PopupMenuItem(
    child: Text('Sign Up'),
    ),
    PopupMenuItem(
    child: Text('Subscribe'),
    ),
    ],
    tooltip: "Press for additional options",
    ),
    ],
    ),
       body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
              padding: EdgeInsets.all(20.0),
              child: Center(
                child: Container(
                  padding: EdgeInsets.all(20.0),
                  decoration: BoxDecoration(
                      color: const Color(0xFF9B0000),
                      borderRadius: BorderRadius.circular(16)
                  ),
                  child: Hero(
                    tag: "${widget.product.id}",
                    child: Image.asset("${widget.product.image}"),
                  ),
                ),
              ),
          ),

          Center(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 16),
              child: Text(
                  widget.product.title,
                  style: TextStyle(color: Colors.grey)),
            ),
          ),
          Center(
            child: Text(
              "\$${widget.product.price}",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          Center(
            child: Padding(
                padding: EdgeInsets.all(20),
                child: Text("${widget.product.desc}", textAlign: TextAlign.center, style: TextStyle(fontSize: 16.0),),
              ),
            ),

          Center(
            child: widget.product.addedToCart ?
            RaisedButton(
              child: Text("Remove from Cart", style: TextStyle(fontWeight: FontWeight.bold),),
              onPressed: () {
                setState(() {
                  widget.product.addedToCart = false;
                });
                addedProducts.removeWhere((element) => element.id == widget.product.id);
                final showSnackBar = SnackBar(
                  content: Text('This Item has been removed from your Cart'),
                  duration: const Duration(seconds: 3),
                  behavior: SnackBarBehavior.floating,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30))),
                  action: SnackBarAction(
                    label: 'Dismiss',
                    onPressed: () {
                      //Perform action here
                    },
                  ),
                );
                // Find the Scaffold in the widget tree and use
                // it to show a SnackBar.
                Scaffold.of(context).showSnackBar(showSnackBar);
              },
            )
                : RaisedButton(
                child: Text("Add to cart"),
                onPressed: (){
                  setState(() {
                    widget.product.addedToCart = true;
                    addedProducts.add(widget.product);
                  });
                  final showSnackBar = SnackBar(
                    content: Text('This item has been added to your Cart!'),
                    duration: const Duration(seconds: 3),
                    behavior: SnackBarBehavior.floating,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(30))),
                    action: SnackBarAction(
                      label: 'Dismiss',
                      onPressed: () {
                      },
                    ),
                  );

                  // Find the Scaffold in the widget tree and use
                  // it to show a SnackBar.
                  Scaffold.of(context).showSnackBar(showSnackBar);
                }
            ),

          ),

        ],

    ),
     );
  }

}