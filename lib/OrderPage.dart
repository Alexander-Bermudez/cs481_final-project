import 'dart:async';
import 'package:final_project/orderCompleted.dart';
import 'package:final_project/product/Product.dart';
import 'package:final_project/product/ProductView.dart';
import 'product/ProductList.dart';
import 'package:flutter/material.dart';

class OrderPage extends StatefulWidget {
  final Product product;
  OrderPage({Key key, this.product}):super(key: key);
  @override
  State<StatefulWidget> createState() {
    return OrderPageState();
  }
}

class OrderPageState extends State<OrderPage> {
  bool isLoading = false;
  bool ordered = false;
  FutureOr onGoBack(dynamic value) {
    setState(() {});
  }
  void _sending() {
    setState(() {
      isLoading = true;
    });
    Timer.periodic(const Duration(seconds: 3), (t) {
      setState(() {
        isLoading = false;
        ordered = true;
      });
      t.cancel();
    });
  }
  void orderIsCompleted() {
    ordersCompleted++;
    orderTracker = true;
  }
  @override
  OrderPage get widget => super.widget;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text("Your Order")
        ),
        body: Column(
          children: <Widget>[
            addedProducts.isEmpty ?
            Column(
                children: [
                  Center(
                    child: Image.asset('images/no-orders.png'),),
                  Text("You don't have any items in your cart to place an order yet."),
                ]
            )
                : Expanded(
              child: ListView.builder(
                itemCount: addedProducts.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    title: ProductView(product: addedProducts[index]),
                  );
                },
                scrollDirection: Axis.vertical,
              ),
            ),
            if (isLoading == true) CircularProgressIndicator(
              valueColor: new AlwaysStoppedAnimation<Color>(Colors.red),
            ),
            if (isLoading == true)
              Text(
                'CONFIRMING YOUR ORDER...',
                style: TextStyle(fontSize: 17),
              ),
            if (ordered == true)
              Text(
                'YOUR ORDER HAS BEEN CONFIRMED',
                style: TextStyle(fontSize: 17),
              ),
            addedProducts.isEmpty == false && ordered == false ?
            Container(
                height: 40.0,
                padding: EdgeInsets.all(3),
                child:
                Material(
                  borderRadius: BorderRadius.circular(20.0),
                  shadowColor: Colors.black,
                  color: Color(0xFF9B0000),
                  elevation: 7.0,
                  child: GestureDetector(
                    onTap: () {
                      orderIsCompleted();
                      _sending();
                    },
                    child: Center(
                      child: Text(
                        'Confirm Order',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Montserrat'),
                      ),
                    ),
                  ),
                )
            ) : Container(
              height: 0,
            ),
            addedProducts.isEmpty == true && ordered == false ?
            Container(
              height: 40.0,
              padding: EdgeInsets.all(3),
              child: Material(
                borderRadius: BorderRadius.circular(20.0),
                shadowColor: Colors.black,
                color: Color(0xFF9B0000),
                elevation: 7.0,
                child: GestureDetector(
                  onTap: () {
                    Navigator.pushReplacementNamed(context, '/home');
                  },
                  child: Center(
                    child: Text(
                      'Shop for Items to Add to your Cart',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Montserrat'),
                    ),
                  ),
                ),
              ),
            ) : Container(
              height: 0,
            ),
            if(ordered)
              Container(
                height: 40.0,
                padding: EdgeInsets.all(3),
                child: Material(
                  borderRadius: BorderRadius.circular(20.0),
                  shadowColor: Colors.black,
                  color: Color(0xFF9B0000),
                  elevation: 7.0,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pushReplacementNamed(context, '/order_complete');
                    },
                    child: Center(
                      child: Text(
                        'View your Order',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Montserrat'),
                      ),
                    ),
                  ),
                ),
              ),
          ],
        )
    );
  }
}