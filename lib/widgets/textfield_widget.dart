import 'package:flutter/material.dart';

class TextFieldWidget extends StatelessWidget {
  final String hintText;
  final bool isPassword;
  final IconData prefixIconData;
  final Function onChanged;

  TextFieldWidget({
    this.hintText,
    this.isPassword,
    this.prefixIconData,
    this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    return TextField(
      onChanged: onChanged,
      style: TextStyle(
        color: Color(0xFF9B0000),
        fontSize: 14.0,
      ),
      decoration: InputDecoration(
        hintText: hintText,
        prefixIcon: Icon(
          prefixIconData,
          size: 18,
          color: Color(0xFF9B0000),
        ),
        filled: true,
        enabledBorder: UnderlineInputBorder(
          borderRadius: BorderRadius.circular(10),
          borderSide: BorderSide.none,
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
          borderSide: BorderSide(color: Color(0xFF9B0000)),
        ),
        focusColor: Color(0xFF9B0000),
        labelStyle: TextStyle(color: Color(0xFF9B0000)),
      ),
    );
  }
}
