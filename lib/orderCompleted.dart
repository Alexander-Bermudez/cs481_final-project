import 'dart:async';
import 'dart:math';
import 'package:final_project/product/ProductList.dart';
import 'package:flutter/material.dart';
import 'package:final_project/product/orders.dart';


class OrderCompletedPage extends StatefulWidget {

  OrderCompletedPage({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return OrderCompletedPageState();
  }
}

class OrderCompletedPageState extends State<OrderCompletedPage> {
  FutureOr onGoBack(dynamic value) {
    setState(() {});
  }
  void clearItems() {
    if(orderTracker) {
      Random random = new Random();
      int randomNumber = random.nextInt(6) + 2;
      String amountItems = addedProducts.length.toString();
      String totalCost = "Total Cost: \$${addedProducts.fold(
          0, (a, b) => a + b.price).toStringAsFixed(2)}";
      orderedProducts.add(new Orders(
        totalItems: amountItems,
        cost: totalCost,
        arrival: randomNumber.toString(),));
      for (int i = 0; i < addedProducts.length - 1; i++) {
        addedProducts[i].addedToCart = false;
      }
      addedProducts.clear();
      orderTracker = false;
    }
  }
  @override
  Widget build(BuildContext context) {

    clearItems();
    return new Scaffold(
        appBar: AppBar(
            title: Text("Your Orders")
        ),
        body: Column(
          children: <Widget> [
            orderedProducts.isEmpty ?
            Column(
                children: [
                  Center(
                    child: Image.asset('images/no-orders.png'),),
                  Text("It looks like you don't have any orders yet."),
                ]
            )
                : Expanded(
              child: ListView.builder(
                itemCount: orderedProducts.length,
                itemBuilder: (context, index) {
                  return Card(
                    child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ListTile(
                            leading: Icon(Icons.shopping_basket),
                            title: Text('Total Items: ' + orderedProducts[index].totalItems),
                            subtitle: Text('Total Cost: ' + orderedProducts[index].cost + '\nArriving in ' + orderedProducts[index].arrival + ' days.'),
                          ),
                        ],
                    ),
                  );
                },
                scrollDirection: Axis.vertical,
              ),
            ),
            Container(
              height: 40.0,
              padding: EdgeInsets.all(5),
              child: Material(
                borderRadius: BorderRadius.circular(20.0),
                shadowColor: Colors.black,
                color: Color(0xFF9B0000),
                elevation: 7.0,
                child: GestureDetector(
                  onTap: () {
                    Navigator.pushReplacementNamed(context, '/home');
                  },
                  child: Center(
                    child: Text(
                      'Return to Home',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Montserrat'),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
    );
  }

}