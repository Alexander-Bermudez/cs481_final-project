import 'dart:async';
import 'package:final_project/ErrorPage.dart';
import 'package:final_project/SignUpPage.dart';
import 'package:final_project/orderCompleted.dart';
import 'package:flutter/material.dart';
import 'OrderPage.dart';
import 'product/Product.dart';
import 'product/ProductView.dart';
import 'product/ProductList.dart';
import 'SplashPage.dart';
import 'CartPage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final String ourAppTitle = 'Totally Not Amazon';
    const PrimaryColor = Color(0xFF9B0000);
    return MaterialApp(
      title: ourAppTitle,
      theme: ThemeData(
        primaryColor: PrimaryColor,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => BuildSplashPage(),
        '/home': (context) => BuildHomePageState(),
        '/cart_page': (context) => CartPage(),
        '/order': (context) => OrderPage(),
        '/order_complete': (context) => OrderCompletedPage(),
        '/signup': (context) => BuildSignUpPage(),
        '/forgot_password': (context) => BuildErrorPage(),
      },
      //BuildHomePage(title: ourAppTitle),
    );
  }
}

class BuildHomePageState extends StatefulWidget {
  @override
  BuildHomePage createState() => BuildHomePage();
}

class BuildHomePage extends State<BuildHomePageState> {
  final List<Product> headwear = [
    new Product(
        image: "images/bean.jpg",
        title: "Fluffy Beanie",
        desc: "Wanna show off just how soft and fluffy your head can be? Well this is just the beanie for you!",
        price: 25.99,
        size: 20,
        id: 1,
        addedToCart: false),
    new Product(
        image: "images/hat.jpg",
        title: "Wide Brim Floppy Hat",
        desc: "Wanna show off just how hip and non-mainstream you are? Well this floppy hat is just for you!",
        price: 19.99,
        size: 20,
        id: 2,
        addedToCart: false),
    new Product(
        image: "images/beanie-grey.jpg",
        title: "Grey Beanie",
        desc: "Grey Skater Beanie",
        price: 27.99,
        size: 20,
        id: 3,
        addedToCart: false),
    new Product(
        image: "images/captain.jpg",
        title: "Captain Hat",
        desc:
            "Sail the seven seas with this blue captain's hat! Feel like a true sailor.",
        price: 34.99,
        size: 20,
        id: 4,
        addedToCart: false),
    new Product(
        image: "images/wolf-hat.png",
        title: "Wolf-Logo Hat",
        desc:
            "Feel like a true dad with this wolf-themed cap! (does not include actual wolf).",
        price: 19.99,
        size: 20,
        id: 5,
        addedToCart: false),
    new Product(
        image: "images/golf-hat.jpg",
        title: "Golf Hat",
        desc:
            "Feel like the amazing Tiger Woods as you play golf wearing this white golfing hat! (does not guarantee actually being good at golf).",
        price: 44.99,
        size: 20,
        id: 6,
        addedToCart: false),
  ];

  final List<Product> electronics = [
    new Product(
        image: "images/speaker.jpg",
        title: "Bluetooth Speaker",
        desc:
            "A white and green bluetooth speaker with a built-in subwoofer on top.",
        price: 49.99,
        size: 20,
        id: 7,
        addedToCart: false),
    new Product(
        image: "images/macbook.jpg",
        title: "2020 Macbook Pro",
        desc:
            "A brand-spanking new Macbook Pro with upgraded CPU, battery-life, as well as a brand new touch screen above the keyboard for added functionality!",
        price: 1899.99,
        size: 20,
        id: 8,
        addedToCart: false),
    new Product(
        image: "images/projector.png",
        title: "Projector",
        desc:
            "A brand new projector for displaying things wherever you like! Comes with additional remote for ease of use.",
        price: 119.99,
        size: 20,
        id: 9,
        addedToCart: false),
    new Product(
        image: "images/iphone.jpg",
        title: "IPhone 12 Pro",
        desc:
            "Get the brand new IPhone 12 Pro today to receive yet another IPhone with barely ANY improvements! It's great to show to your friends so they can see how rich you are!",
        price: 989.99,
        size: 20,
        id: 10,
        addedToCart: false),
    new Product(
        image: "images/earbuds.jpg",
        title: "Bluetooth Earbuds",
        desc:
            "Bluetooth earbuds that hang around your neck when you're not using them! Boasts great connectivity for listening to music on the go, all wirelessly!",
        price: 79.99,
        size: 20,
        id: 11,
        addedToCart: false),
  ];
  final List<Product> shirts = [
    new Product(
        image: "images/shirt-green.jpg",
        title: "Green Short-sleeve T-shirt",
        desc:
            "A green short-sleeve shirt that we guarantee will make your muscles look HUGE!(not liable for any small muscles)",
        price: 39.99,
        size: 20,
        id: 12,
        addedToCart: false),
    new Product(
        image: "images/shortSleeve.jpg",
        title: "Black Short-sleeve T-shirt",
        desc: "Perfect for showing how absolutely normal and boring you are!",
        price: 29.99,
        size: 20,
        id: 13,
        addedToCart: false),
    new Product(
        image: "images/flannel.jpg",
        title: "Heavy-duty Flannel",
        desc:
            "Become a lumberjack, or get through any cold weather with this heavy-duty flannel!",
        price: 54.99,
        size: 20,
        id: 14,
        addedToCart: false),
    new Product(
        image: "images/polo.jpg",
        title: "White Polo Shirt",
        desc:
            "Don't wanna over-dress for the party but also don't wanna under-dress? Well have I got the perfect solution for you. This white polo shirt! The best way of showing that you don't care while still being business-casual.",
        price: 27.99,
        size: 20,
        id: 15,
        addedToCart: false),
    new Product(
        image: "images/LongSleeve.jpg",
        title: "Black Long-sleeve T-shirt",
        desc: "Perfect for showing how absolutely normal and boring you are! Even when it's chilly!",
        price: 35.99,
        size: 20,
        id: 16,
        addedToCart: false),
  ];
  final List<Product> pants = [
    new Product(
        image: "images/jeans.jpg",
        title: "Faded Jeans",
        desc:
            "Wanna show how hip you are? Well faded jeans are all the rage right now! Get some new faded Jeans today!",
        price: 69.99,
        size: 20,
        id: 17,
        addedToCart: false),
    new Product(
        image: "images/joggers.jpeg",
        title: "Joggers",
        desc:
            "Want something comfortable for both working out and for lounging around the house being lazy? Well joggers are perfect for you!",
        price: 49.99,
        size: 20,
        id: 18,
        addedToCart: false),
    new Product(
        image: "images/summer-pants.jpg",
        title: "Summer Pants",
        desc:
            "Some nice, colorful pants for a day out on the town during the summer.",
        price: 54.99,
        size: 20,
        id: 19,
        addedToCart: false),
    new Product(
        image: "images/cargo.png",
        title: "Cargo Pants",
        desc:
            "Got way too much stuff and not enough pockets in your normal pants? Well cargo pants are just the thing for you!",
        price: 37.99,
        size: 20,
        id: 20,
        addedToCart: false),
    new Product(
        image: "images/sweats.jpg",
        title: "Grey Sweats",
        desc:
            "Perfect for lounging around the house and doing absolutely nothing!",
        price: 25.99,
        size: 20,
        id: 21,
        addedToCart: false),
  ];

  final List<Product> jewelry = [
    new Product(
        image: "images/patrick-necklace.jpg",
        title: "Patrick Gold and Diamond Chain",
        desc:
        "Wanna show off all your drip with a beautiful chain, but still wanna act like a child? Well, this really expensive and overpriced Patrick Star is for you!",
        price: 2100.99,
        size: 20,
        id: 22,
        addedToCart: false),
    new Product(
        image: "images/diamond-ring.jpg",
        title: "Diamond Ring",
        desc:
        "Wanna propose to your partner but don't have the money? Well this fake diamond ring is just the thing for you!",
        price: 49.99,
        size: 20,
        id: 23,
        addedToCart: false),
    new Product(
        image: "images/earring.jpg",
        title: "Diamond Stud Earring",
        desc:
        "Wanna show off to your friends how rich you are? This earring is just the thing for you!",
        price: 599.99,
        size: 20,
        id: 24,
        addedToCart: false),
    new Product(
        image: "images/gold-necklace.jpg",
        title: "Gold Necklace",
        desc:
        "Perfect for that first date or if you're going out on the town on a girl's night!",
        price: 379.99,
        size: 20,
        id: 25,
        addedToCart: false),
    new Product(
        image: "images/red-ring.jpg",
        title: "Red Stone Silver Ring",
        desc:
        "Perfect for showing off how EDGY you are with this red stone ring! even though it's way too big!",
        price: 129.99,
        size: 20,
        id: 26,
        addedToCart: false),
  ];

  FutureOr onGoBack(dynamic value) {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Totally not Amazon'),
        actions: <Widget>[
          PopupMenuButton(
            itemBuilder: (context) => [
              PopupMenuItem(
                child: GestureDetector(
                    onTap: () {
                      Navigator.pushReplacementNamed(context, '/');
                    },
                    child: Text('Logout')),
              ),
            ],
            tooltip: "Press for additional options",
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: "View current total cost",
        child: Icon(Icons.attach_money),
        backgroundColor: Color(0xFF9B0000),
        onPressed: () {
          showModalBottomSheet<void>(
            context: context,
            builder: (BuildContext context) {
              return Container(
                height: 200,
                color: Color(0xFF9B0000),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      const Text(
                        'Current Total Cost from Cart',
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                      Text(
                        "Total: \$${addedProducts.fold(0, (a, b) => a + b.price).toStringAsFixed(2)}",
                        style: TextStyle(color: Colors.white, fontSize: 16),
                      ),
                      RaisedButton(
                        child: const Text('Close'),
                        onPressed: () => Navigator.pop(context),
                      )
                    ],
                  ),
                ),
              );
            },
          );
        },
      ),
      body: SingleChildScrollView(
        child: Column(children: <Widget>[
          Text('Electronics',
              style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold)),
          Container(
            height: 420,
            child: ListView.builder(
              itemCount: electronics.length,
              itemBuilder: (context, index) {
                return ProductView(product: electronics[index]);
              },
              scrollDirection: Axis.horizontal,
            ),
          ),
          Text('Jewelry',
              style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold)),
          Container(
            height: 420,
            child: ListView.builder(
              itemCount: jewelry.length,
              itemBuilder: (context, index) {
                return ProductView(product: jewelry[index]);
              },
              scrollDirection: Axis.horizontal,
            ),
          ),
          Text('Headwear',
              style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold)),
          Container(
            height: 420,
            child: ListView.builder(
              itemCount: headwear.length,
              itemBuilder: (context, index) {
                return ProductView(product: headwear[index]);
              },
              scrollDirection: Axis.horizontal,
            ),
          ),
          Text('Shirts',
              style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold)),
          Container(
            height: 420,
            child: ListView.builder(
              itemCount: shirts.length,
              itemBuilder: (context, index) {
                return ProductView(product: shirts[index]);
              },
              scrollDirection: Axis.horizontal,
            ),
          ),
          Text('Pants',
              style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold)),
          Container(
            height: 420,
            child: ListView.builder(
              itemCount: pants.length,
              itemBuilder: (context, index) {
                return ProductView(product: pants[index]);
              },
              scrollDirection: Axis.horizontal,
            ),
          ),
        ]),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: <Color>[
                    Color(0xFF9B0000),
                    Colors.white,
                  ]),
              ),
              child: Container(
                child: Column(
                  children: <Widget>[
                    Material(
                      borderRadius: BorderRadius.all(Radius.circular(50.0)),
                      elevation: 10,
                      child: Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Image.asset('images/profile_placeholder.png',width: 100, height: 100),
                      ),
                    )
                  ],
                ),
              ),
            ),
            ListTile(
              leading: Icon(Icons.home),
              title: Text('Homepage'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(Icons.shopping_cart),
              title: Text('View Cart'),
              onTap: () {
                Navigator.pushNamed(
                  context,
                  '/cart_page',
                ).then(onGoBack);
              },
            ),
            ListTile(
                leading: Icon(Icons.shopping_basket),
                title: Text('Checkout'),
                onTap: () {
                  Navigator.pushNamed(
                    context,
                    '/order',
                  ).then(onGoBack);
                }
            ),
            ListTile(
              leading: Icon(Icons.local_offer),
              title: Text('View Orders'),
              onTap: () {
                Navigator.pushNamed(
                  context,
                  '/order_complete',
                ).then(onGoBack);
              },
            ),
          ],
        ),
      ),
    );
  }
}
