import 'dart:async';
import 'package:final_project/product/Product.dart';
import 'package:final_project/product/ProductView.dart';
import 'product/ProductList.dart';
import 'package:flutter/material.dart';
import 'OrderPage.dart';

class CartPage extends StatefulWidget {

  final Product product;
  CartPage({Key key, @required this.product}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return CartPageState();
  }
}

class CartPageState extends State<CartPage> {

  FutureOr onGoBack(dynamic value) {
    setState(() {});
  }

  @override
  CartPage get widget => super.widget;

  @override
  Widget build(BuildContext context) {

    if(widget.product != null){
      addedProducts.add(widget.product);
    }

    return Scaffold(
      appBar: AppBar(
          title: Text("Your Cart")
      ),
      body: Column(
        children: <Widget> [
          addedProducts.isEmpty ?
          Column(
              children: [
                Center(
                child: Image.asset('images/no-orders.png'),),
                Text("It looks like you don't have any items in your cart yet."),
              ]
          )
          : Expanded(
            child: ListView.builder(
              itemCount: addedProducts.length,
              itemBuilder: (context, index) {
                return ListTile(
                  title: ProductView(product: addedProducts[index]),
                );
              },
              scrollDirection: Axis.vertical,
            ),
          ),
          Padding(
              padding: const EdgeInsets.all(15),
              child: Column( children: <Widget> [
                Container(
                  height: 75,
                  width: 100,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    color: const Color(0xFF9B0000)
                  ),
                  child: Center(
                    child: Text(
                      "Total: \$${addedProducts.fold(0, (a,b) => a + b.price).toStringAsFixed(2)}",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
                Container(
                    child: addedProducts.isEmpty == false ?
                    RaisedButton(
                      onPressed: () {Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => OrderPage())
                      ).then(onGoBack);
                      },
                      child: Text("Checkout"),
                    )
                        :RaisedButton(
                      onPressed: () {Navigator.of(context).pop();
                      },
                      child: Text("View Items to Add to your Cart"),
                    )
                ),
              ],),
          )
        ],
      )
    );
  }

}